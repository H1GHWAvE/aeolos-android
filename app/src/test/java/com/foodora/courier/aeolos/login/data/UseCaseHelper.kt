package com.foodora.courier.aeolos.login.data

import org.assertj.core.api.Assertions

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 15.08.17.
 */


fun verifyResult(error: NetworkError, expectedError: NetworkError?) {
    Assertions.assertThat(error).isEqualTo(expectedError)
}