package com.foodora.courier.aeolos

import java.util.concurrent.Executor


/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 09.08.17.
 */

class TestExecutor : Executor {
    override fun execute(command: Runnable) {
        command.run()
    }
}