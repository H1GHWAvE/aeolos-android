package com.foodora.courier.aeolos.login.data

import android.content.Context
import com.foodora.courier.aeolos.MockWebServerTestBase
import com.foodora.courier.aeolos.R
import com.foodora.courier.aeolos.TestExecutor
import com.foodora.courier.aeolos.login.domain.db.EndpointDao
import com.foodora.courier.aeolos.login.domain.entities.Endpoint
import com.foodora.courier.aeolos.login.domain.mappers.CountriesMapper
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.doReturn
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.doNothing
import org.mockito.MockitoAnnotations


/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 09.08.17.
 */

class EndpointsDownloadUseCaseTest : MockWebServerTestBase() {

    // 1. setup conditions for test...
    // 2. Execute code under test...
    // 3. Make assertions on the result...

    @Mock private lateinit var brandsDao: BrandsDao
    @Mock private lateinit var context: Context
    @Mock private lateinit var endpointDao: EndpointDao

    private lateinit var useCaseEndpoints: EndpointsDownloadUseCase

    private var brandsMapper: BrandsMapper = BrandsMapper()
    private var countriesMapper: CountriesMapper = CountriesMapper()
    private var expectedError: Error? = null

    private val brandsCaptor = argumentCaptor<List<Brand>>()
    private val countriesCaptor = argumentCaptor<List<Endpoint>>()
    private val executor = TestExecutor()


    //region Before

    @Before
    @Throws(Exception::class)
    override fun setUp() {
        super.setUp()
        MockitoAnnotations.initMocks(this)

        useCaseEndpoints = EndpointsDownloadUseCase(brandsDao, brandsMapper, httpClient, endpointDao, countriesMapper, executor)

        doNothing().`when`(brandsDao).insert(brandsCaptor.capture())
        doNothing().`when`(endpointDao).insert(countriesCaptor.capture())

        doReturn("https://s3-eu-west-1.amazonaws.com/foodora-dispatcher/").`when`(context).getString(R.string.url_endpoints)
        doReturn("mock_endpointshen`(context).getString(R.string.url_endpoints_json)
    }

    //endregion


    //region Test

    @Test
    @Throws(Exception::class)
    fun getHappyPath() {
        enqueueMockResponse(200, "test_endpoints.json")
        expectedError = Error.SUCCESS

        useCaseEndpoints.get({ verifyResult(it, expectedError) })

        assertThat(brandsCaptor.firstValue[0].name).isEqualTo("ClickDelivery")
        assertThat(countriesCaptor.firstValue[0].countryCode).isEqualTo("au")
    }

    @Test
    @Throws(Exception::class)
    fun getErrorPath() {
        enqueueMockResponse(400, "test_endpoints.json")
        expectedError = NetworkError.UNKNOWN

        useCaseEndpoints.get({ verifyResult(it, expectedError) })
    }

    //endregion

}