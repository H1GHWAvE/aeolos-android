package com.foodora.courier.aeolos.login.presentation.brand

import com.foodora.courier.aeolos.base.common.mapper.Mapper
import com.foodora.courier.aeolos.base.presentation.recyclerview.DisplayableItem
import com.foodora.courier.aeolos.base.presentation.viewholder.createSpacer
import com.foodora.courier.aeolos.login.presentation.LoginConstants
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

class BrandLinearDisplayableItemMapper
@Inject
constructor() : Mapper<List<DisplayableItem<Any>>, List<BrandViewEntity>> {

    @AddTrace(name = "login_brand_display_map_linear")
    override fun map(input: List<BrandViewEntity>): List<DisplayableItem<Any>> {
        val countries = mutableListOf<DisplayableItem<Any>>()

        countries.add(createSpacer())
        input.mapTo(countries) {
            DisplayableItem(
                    model = it,
                    type = LoginConstants.DisplayableTypes.BRAND_LINEAR
            )
        }
        countries.add(createSpacer())

        return countries
    }

}