package com.foodora.courier.aeolos.login.presentation.endpoint

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 22.10.17.
 */

data class EndpointViewEntity(val brand: String,
                              val flag: Int)
