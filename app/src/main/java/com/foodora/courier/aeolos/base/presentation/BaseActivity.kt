package com.foodora.courier.aeolos.base.presentation

import android.app.Activity
import android.arch.lifecycle.LifecycleRegistry
import android.content.Context
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v7.app.AppCompatActivity

/**
 * Created by H1GHWAvE on 26/05/16.
 */

abstract class BaseActivity :
        AppCompatActivity() {

    private val lifecycleRegistry = LifecycleRegistry(this)

    //region lifecycle

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //overridePendingTransition(R.anim.go_in, R.anim.go_out)
    }

    //endregion


    //region android

    @CallSuper
    override fun onBackPressed() {
        super.onBackPressed()
        //overridePendingTransition(R.anim.back_in, R.anim.back_out)
    }

    //endregion


    //region helper

    @CallSuper
    override fun getLifecycle(): LifecycleRegistry {
        return lifecycleRegistry
    }

    abstract val activity: Activity

    abstract val context: Context

    //endregion

}
