package com.foodora.courier.aeolos.login.presentation

import com.foodora.courier.aeolos.base.common.provider.FirebaseEventProvider
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.UserInterface.BRAND_SELECT
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.UserInterface.COUNTRY_RESET
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.UserInterface.COUNTRY_SELECT
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.UserInterface.ENDPOINT_RESET
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.UserInterface.LOGIN
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.UserInterface.REFRESH
import com.google.firebase.perf.metrics.AddTrace
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

class LoginPresenter
@Inject
constructor(private val firebaseEvent: FirebaseEventProvider) {

    private lateinit var viewModel: LoginViewModel
    private var decorator: LoginUserInterface? = null

    //region delegate

    private val delegate = object : LoginUserInterface.Delegate {

        override fun onRefresh() {
            firebaseEvent.log(REFRESH)
            viewModel.refreshEndpoints()
        }

        override fun onBrandSelect(brand: String) {
            firebaseEvent.log(BRAND_SELECT, brand)
            viewModel.selectBrand(brand)
        }

        override fun onCountryReset() {
            firebaseEvent.log(COUNTRY_RESET)
            viewModel.reset()
        }

        override fun onCountrySelect(countryCode: String) {
            firebaseEvent.log(COUNTRY_SELECT, countryCode)
            viewModel.selectCountryCode(countryCode)
        }

        override fun onEndpointReset() {
            firebaseEvent.log(ENDPOINT_RESET)
            viewModel.reset()
        }

        override fun onLogin() {
            firebaseEvent.log(LOGIN)
            Timber.e("onLogin()")
        }

    }

    //endregion


    //region lifecycle

    @AddTrace(name = "login_presenter_initialize")
    fun initialize(decorator: LoginUserInterface,
                   viewModel: LoginViewModel) {

        this.decorator = decorator
        this.viewModel = viewModel
        this.decorator?.initialize(delegate, viewModel)
    }

    @AddTrace(name = "login_presenter_dispose")
    fun dispose() {
        this.decorator = null
    }

    //endregion

}