package com.foodora.courier.aeolos.app.home.data

import com.foodora.courier.aeolos.BuildConfig
import com.foodora.courier.base.domain.db.SessionDao
import javax.inject.Inject

/**
 * Created by johannes.neutze on 26.09.17.
 */


class SessionRepository
@Inject
constructor(private var sessionDao: SessionDao) {

    private val session get() = sessionDao.getSession()

    //region network

    val authenticationToken: String? get() = token?.let { "Bearer $it" }
    val baseUrl: String get() = BuildConfig.URL_BASE
    val countryConfigJson: String? get() = BuildConfig.URL_JSON_COUNTRY
    val url: String? get() = session.countryUrl
    val token: String? get() = session.token

    //endregion


    //region country

    val country: String get() = session.country ?: "DE"

    //endregion

}