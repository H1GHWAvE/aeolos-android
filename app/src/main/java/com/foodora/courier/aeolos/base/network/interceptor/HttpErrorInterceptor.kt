package com.foodora.courier.aeolos.base.network.interceptor

import com.foodora.courier.aeolos.base.network.exception.ForbiddenException
import com.foodora.courier.aeolos.base.network.exception.HttpException
import com.foodora.courier.aeolos.base.network.exception.UnauthorizedException
import com.google.firebase.perf.metrics.AddTrace
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by johannes.neutze on 28.09.17.
 */

class HttpErrorInterceptor : Interceptor {

    @AddTrace(name = "base_network_interceptor_http")
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        val code = response.code()

        when (code) {
            401 -> throw UnauthorizedException(message = response.message())
            403 -> throw ForbiddenException(message = response.message())
            else -> {
                if (code !in 200..299) {
                    throw HttpException(message = response.message())
                }
            }
        }

        return response
    }

}

