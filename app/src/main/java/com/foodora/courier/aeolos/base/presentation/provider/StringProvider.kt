package com.foodora.courier.aeolos.base.presentation.provider

import android.content.Context
import android.support.annotation.StringRes
import com.foodora.courier.aeolos.base.common.extension.StringUtils
import javax.inject.Inject


/**
 * Created by johannes.neutze on 20.09.17.
 */

class StringProvider
@Inject
internal constructor(private val context: Context, private val stringUtils: StringUtils) {

    fun getString(@StringRes resId: Int): String {
        return context.getString(resId)
    }

    fun getString(@StringRes resId: Int, vararg formatArgs: Any): String {
        return context.getString(resId, formatArgs)
    }

    /**
     * Use to replace the placeholders for strings that use the format "text {{placeholder}} text".
     * @param stringResId   string resource id
     * @param substitutions substitutions
     * @return string
     */
    fun getStringAndApplySubstitutions(@StringRes stringResId: Int, vararg substitutions: Pair<String, String>): String {
        return stringUtils.applySubstitutionsToString(context.getString(stringResId), *substitutions)
    }
}