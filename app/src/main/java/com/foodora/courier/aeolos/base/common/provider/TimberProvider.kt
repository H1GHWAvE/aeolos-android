package com.foodora.courier.aeolos.base.common.provider

import com.foodora.courier.aeolos.BuildConfig
import com.google.firebase.perf.metrics.AddTrace
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 23.10.17.
 */

class TimberProvider
@Inject
internal constructor() {

    @AddTrace(name = "base_common_provider_timber")
    fun init() {
        if (BuildConfig.DEBUG) {
            Timber.plant(object : Timber.DebugTree() {
                override fun createStackElementTag(element: StackTraceElement): String {
                    return super.createStackElementTag(element) + ':' + element.lineNumber
                }
            })
        }
    }

}