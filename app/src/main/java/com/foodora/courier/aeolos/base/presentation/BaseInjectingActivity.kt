package com.foodora.courier.aeolos.base.presentation

import android.os.Bundle
import android.support.annotation.CallSuper
import com.foodora.courier.aeolos.base.common.precondition.Preconditions


/**
 * Created by johannes.neutze on 20.09.17.
 */

abstract class BaseInjectingActivity<Component> : BaseActivity() {

    private var component: Component? = null

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        component = createComponent()
        onInject(component!!)

        super.onCreate(savedInstanceState)
    }

    fun getComponent(): Component {
        return Preconditions[component]
    }

    protected abstract fun onInject(component: Component)

    protected abstract fun createComponent(): Component

}