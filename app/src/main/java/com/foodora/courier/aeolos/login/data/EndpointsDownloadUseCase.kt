package com.foodora.courier.aeolos.login.data


import com.foodora.courier.aeolos.BuildConfig
import com.foodora.courier.aeolos.base.common.alias.Callback
import com.foodora.courier.aeolos.base.common.provider.FirebaseEventProvider
import com.foodora.courier.aeolos.base.data.NetworkStatus
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.UseCase.ENDPOINTS_DOWNLOAD
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.UseCase.ENDPOINTS_DOWNLOAD_ERROR
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.UseCase.ENDPOINTS_DOWNLOAD_SUCCESS
import com.foodora.courier.aeolos.login.domain.db.EndpointDao
import com.foodora.courier.aeolos.login.domain.mappers.CountriesMapper
import com.foodora.courier.aeolos.login.network.service.EndpointService
import com.google.firebase.perf.metrics.AddTrace
import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executor
import javax.inject.Inject

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 07.08.17.
 */

class EndpointsDownloadUseCase
@Inject
constructor(private val countriesMapper: CountriesMapper,
            private val endpointDao: EndpointDao,
            private val executor: Executor,
            private val firebaseEventProvider: FirebaseEventProvider,
            private val httpClient: OkHttpClient) {

    @AddTrace(name = "login_data_endpoints_download")
    fun get(callback: Callback<NetworkStatus>) {
        callback(NetworkStatus.LOADING)

        firebaseEventProvider.log(ENDPOINTS_DOWNLOAD)

        executor.execute {
            try {
                val amazonService = retrofit2.Retrofit.Builder()
                        .client(httpClient)
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(BuildConfig.URL_BASE)
                        .build()
                        .create(EndpointService::class.java)

                val response = amazonService.getEndpoints(BuildConfig.URL_JSON_COUNTRY).execute()
                val endpoints = response.body()
                val countries = countriesMapper.map(endpoints)

                endpointDao.clear()
                endpointDao.insert(countries)

                //TODO (jn): tracking
                callback(NetworkStatus.SUCCESS)
                firebaseEventProvider.log(ENDPOINTS_DOWNLOAD_SUCCESS, countries)
            } catch (e: Throwable) {
                //TODO (jn): tracking
                firebaseEventProvider.log(ENDPOINTS_DOWNLOAD_ERROR, e)
                when (e) {
                    else -> callback(NetworkStatus.UNKNOWN)
                }
            }
        }
    }
}