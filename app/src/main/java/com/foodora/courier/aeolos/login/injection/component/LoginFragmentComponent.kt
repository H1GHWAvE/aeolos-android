package com.foodora.courier.aeolos.login.injection.component

import com.foodora.courier.aeolos.base.injection.scope.FragmentScope
import com.foodora.courier.aeolos.login.injection.module.LoginFragmentModule
import com.foodora.courier.aeolos.login.presentation.LoginFragment
import dagger.Subcomponent







/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

@FragmentScope
@Subcomponent(modules = arrayOf(LoginFragmentModule::class))
interface LoginFragmentComponent {

    fun inject(fragment: LoginFragment)

    interface LoginComponentCreator {

        fun createLoginFragmentComponent(): LoginFragmentComponent
    }

}