package com.foodora.courier.aeolos.base.presentation.util

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject


/**
 * Created by johannes.neutze on 20.09.17.
 */

class ViewModelUtil
@Inject
constructor() {

    @AddTrace(name = "base_util_viewModel_createFor")
    fun <T : ViewModel> createFor(viewModel: T): ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {

            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                if (modelClass.isAssignableFrom(viewModel::class.java)) {
                    return viewModel as T
                }
                throw IllegalArgumentException("unexpected viewModel class " + modelClass)
            }
        }
    }

}