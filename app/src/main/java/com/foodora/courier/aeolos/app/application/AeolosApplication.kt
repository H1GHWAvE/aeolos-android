package com.foodora.courier.aeolos.app.application

import android.app.Application
import android.content.Context
import android.support.annotation.CallSuper
import android.support.v7.app.AppCompatDelegate
import com.foodora.courier.aeolos.app.home.data.FirebaseRemoteConfig
import com.foodora.courier.aeolos.base.common.provider.LeakCanaryProvider
import com.foodora.courier.aeolos.base.common.provider.TimberProvider
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject


/**
 * Created by H1GHWAvE on 26/05/16. https://github.com/frogermcs/DaggerExample
 */

class AeolosApplication : Application() {

    companion object {

        init {
            //TODO (jn): NightMode
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }

        operator fun get(context: Context): AeolosApplication {
            return context.applicationContext as AeolosApplication
        }

    }

    @Inject lateinit var leakCanary: LeakCanaryProvider
    @Inject lateinit var firebaseRemoteConfig: FirebaseRemoteConfig
    @Inject lateinit var timber: TimberProvider

    var component: ApplicationComponent? = null

    //region


    //region lifecycle

    @CallSuper
    @AddTrace(name = "app_aeolos_onCreate")
    override fun onCreate() {
        super.onCreate()
        getApplicationComponent()?.inject(this)

        firebaseRemoteConfig.init()
        leakCanary.init()
        timber.init()
    }

    //endregion


    //region extend

    @AddTrace(name = "app_aeolos_getApplicationComponent")
    private fun getApplicationComponent(): ApplicationComponent? {
        if (component == null) {
            component = DaggerApplicationComponent.builder().application(this).build()
        }
        return component
    }

    //endregion

}