package com.foodora.courier.aeolos.login.domain.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.foodora.courier.aeolos.login.domain.entities.Endpoint

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 07.08.17.
 */

@Dao
abstract class EndpointDao {

    //region insert

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(endpoint: Endpoint): Long

    fun insert(endpoints: List<Endpoint>) {
        for (country in endpoints) {
            insert(country)
        }
    }

    //endregion


    //region delete

    @Query("DELETE FROM countries")
    abstract fun clear()

    //endregion


    //region update

    @Update
    abstract fun update(endpoint: Endpoint)

    //endregion


    //region query

    @Query("SELECT DISTINCT countryCode FROM countries")
    abstract fun getLiveCountries(): LiveData<List<String>>

    @Query("SELECT * FROM countries WHERE countryCode = :countryCode")
    abstract fun getBrands(countryCode: String?): List<Endpoint>

    @Query("SELECT * FROM countries WHERE endpoint = 1")
    abstract fun getLiveEndpoint(): LiveData<Endpoint>

    @Query("SELECT * FROM countries WHERE endpoint = 1")
    abstract fun getEndpoint(): Endpoint

    @Query("SELECT * FROM countries WHERE countryCode = :countryCode AND brand = :brand")
    abstract fun getEndpoint(countryCode: String, brand: String): Endpoint

    //endregion

}