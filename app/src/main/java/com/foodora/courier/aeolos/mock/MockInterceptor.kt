package com.foodora.courier.aeolos.mock

import android.app.Application
import com.google.firebase.perf.metrics.AddTrace
import okhttp3.*
import timber.log.Timber
import java.io.IOException

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 28.09.17.
 */

class MockInterceptor(private var code: Int,
                      private var app: Application) : Interceptor {


    private val headerName = "AcceptDialog"
    private val headerValue = "application/json"

    //region Interceptor

    @AddTrace(name = "mock_network_interceptor_mock")
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val path = originalRequest.url().encodedPath()

        Timber.e("path %s", path)

        return if (path == "a/foodora-dispatcher/country_selection_aeolos.json") {
            val interceptorResponse = Response.Builder()
                    .request(originalRequest)
                    .header(headerName, headerValue)
                    .protocol(Protocol.HTTP_1_1)
                    .message("MockInterceptor")
                    .code(code)
                    .body(getResponseBody())
            interceptorResponse.build()
        } else {
            chain.proceed(originalRequest)
        }
    }

    //endregion


    //region helper

    @AddTrace(name = "mock_network_interceptor_getResponseBody")
    @Throws(IOException::class)
    private fun getResponseBody(): ResponseBody {
        return ResponseBody.create(MediaType.parse("text/plain; charset=UTF-8"), FileHelper.getContentFromFile(app))
    }

    //endregion
}