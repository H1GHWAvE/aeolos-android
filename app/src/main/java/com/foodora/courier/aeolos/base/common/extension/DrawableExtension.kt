package com.foodora.courier.aeolos.base.common.extension

import android.support.annotation.DrawableRes
import com.foodora.courier.aeolos.R
import com.google.firebase.perf.metrics.AddTrace

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

@AddTrace(name = "base_common_extension_drawable_toFlagDrawable")
@DrawableRes
fun String.toFlagDrawable(): Int {
    val resource = "flag_${this.toLowerCase()}"

    return try {
        getIcon(resource)
    } catch (e: Exception) {
        R.drawable.ic_child_friendly_black_24dp
    }
}

@AddTrace(name = "base_common_extension_drawable_toLogoDrawable")
@DrawableRes
fun String.toLogoDrawable(): Int {
    val resource = "logo_${this.toLowerCase()}"

    return try {
        getIcon(resource)
    } catch (e: Exception) {
        R.drawable.ic_child_friendly_black_24dp
    }
}

@AddTrace(name = "base_common_extension_drawable_getIcon")
@DrawableRes
fun getIcon(resource: String): Int {
    return R.drawable::class.java.getField(resource).getInt(null)
}