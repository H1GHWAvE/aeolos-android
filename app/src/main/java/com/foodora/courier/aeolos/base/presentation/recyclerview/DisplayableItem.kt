package com.foodora.courier.aeolos.base.presentation.recyclerview


/**
 * Created by johannes.neutze on 20.09.17.
 */

data class DisplayableItem<T>(var type: Int,
                              var model: T)