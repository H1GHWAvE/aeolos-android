package com.foodora.courier.aeolos.base.presentation.binder

import android.databinding.BindingAdapter
import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.google.firebase.perf.metrics.AddTrace

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 11.07.17.
 */

object DataBindingAdapter {

    @JvmStatic
    @BindingAdapter("android:src")
    @AddTrace(name = "base_common_binding_adapter")
    fun setImageResource(imageView: ImageView, @DrawableRes resource: Int) {
        imageView.setImageResource(resource)
    }

}