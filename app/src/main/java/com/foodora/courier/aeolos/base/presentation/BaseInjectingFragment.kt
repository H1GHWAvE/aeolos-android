package com.foodora.courier.aeolos.base.presentation

import android.content.Context
import android.support.annotation.CallSuper
import android.support.v4.app.Fragment


/**
 * Created by johannes.neutze on 20.09.17.
 */

abstract class  BaseInjectingFragment : Fragment() {

    @CallSuper
    override fun onAttach(context: Context) {
        onInject()

        super.onAttach(context)
    }

    abstract fun onInject()

}
