package com.foodora.courier.aeolos.app.home.presentation

import com.foodora.courier.aeolos.base.injection.module.ActivityModule
import com.foodora.courier.aeolos.base.injection.scope.ActivityScope
import com.foodora.courier.aeolos.login.injection.component.LoginFragmentComponent
import dagger.Subcomponent


/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 04.10.17.
 */

@ActivityScope
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface HomeActivityComponent : LoginFragmentComponent.LoginComponentCreator {

    fun inject(homeActivity: HomeActivity)
}