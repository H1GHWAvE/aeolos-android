package com.foodora.courier.aeolos.base.network.exception

/**
 * Created by johannes.neutze on 28.09.17.
 */

class UnauthorizedException(override val message: String?) : Throwable()
