package com.foodora.courier.aeolos.base.injection.scope

import javax.inject.Scope

/**
 * Created by johannes.neutze on 20.09.17.
 */

@Scope
@kotlin.annotation.Retention
annotation class ActivityScope