package com.foodora.courier.aeolos.base.presentation.viewholder

import com.foodora.courier.aeolos.base.presentation.recyclerview.DisplayableItem
import com.foodora.courier.aeolos.login.presentation.LoginConstants

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 22.10.17.
 */

fun createSpacer(): DisplayableItem<Any> {
    return DisplayableItem(
            model = SpacerViewEntity(),
            type = LoginConstants.DisplayableTypes.SPACER
    )
}