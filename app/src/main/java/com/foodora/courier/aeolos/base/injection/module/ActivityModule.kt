package com.foodora.courier.aeolos.base.injection.module

import android.content.Context
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import com.foodora.courier.aeolos.base.presentation.BaseActivity
import dagger.Module
import dagger.Provides


/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 04.10.17.
 */

@Module
class ActivityModule(private val activity: BaseActivity) {

    @Provides
    internal fun provideBaseActivity(): BaseActivity {
        return activity
    }

    @Provides
    internal fun provideContext(): Context {
        return activity
    }

    @Provides
    internal fun provideFragmentManager(activity: AppCompatActivity): FragmentManager {
        return activity.supportFragmentManager
    }
}