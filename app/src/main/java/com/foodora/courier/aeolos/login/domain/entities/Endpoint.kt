package com.foodora.courier.aeolos.login.domain.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 07.08.17.
 */

@Entity(tableName = "countries")
data class Endpoint(@PrimaryKey(autoGenerate = true) val id: Int = 0,
                    val brand: String,
                    val countryCode: String,
                    val name: String,
                    val url: String,
                    var endpoint: Boolean = false)