package com.foodora.courier.aeolos.login.network.service

import com.foodora.courier.aeolos.login.network.model.EndpointsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 07.08.17.
 */

interface EndpointService {

    @GET("{countrySelectionJson}")
    fun getEndpoints(@Path("countrySelectionJson") countrySelectionJson: String): Call<EndpointsResponse>

}