package com.foodora.courier.aeolos.login.data

import android.arch.lifecycle.LiveData
import com.foodora.courier.aeolos.base.data.NetworkStatus
import com.foodora.courier.aeolos.base.data.NetworkStatusLiveData
import com.foodora.courier.aeolos.login.domain.db.EndpointDao
import com.foodora.courier.aeolos.login.domain.entities.Endpoint
import com.google.firebase.perf.metrics.AddTrace

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 07.08.17.
 */

class LoginRepository
constructor(private val endpointDao: EndpointDao,
            private val endpointsDownloadUseCase: EndpointsDownloadUseCase,
            private val statusData: NetworkStatusLiveData) {

    //TODO(jn): airplane mode?

    //region BrandsDao

    @AddTrace(name = "login_data_repository_getBrands")
    internal fun getBrands(countryCode: String?): List<Endpoint>? {
        return endpointDao.getBrands(countryCode)
    }

    //endregion


    //region EndpointDao

    @AddTrace(name = "login_data_repository_initEndpoints")
    fun initEndpoints(): LiveData<List<String>> {
        endpointDao.clear()
        downloadEndpoints()
        return endpointDao.getLiveCountries()
    }

    @AddTrace(name = "login_data_repository_getEndpoints")
    fun getEndpoints(): LiveData<List<String>> {
        return endpointDao.getLiveCountries()
    }

    @AddTrace(name = "login_data_repository_getEndpoint")
    fun getEndpoint(): LiveData<Endpoint> {
        return endpointDao.getLiveEndpoint()
    }

    @AddTrace(name = "login_data_repository_resetEndpoint")
    fun resetEndpoint() {
       endpointDao.clear()
    }

    @AddTrace(name = "login_data_repository_setEndpoint")
    fun setEndpoint(countryCode: String, brand: String) {
        val endpoint = endpointDao.getEndpoint(countryCode, brand)
        endpoint.endpoint = true
        endpointDao.update(endpoint)
    }

    //endregion


    //region EndpointsDownloadUseCase

    @AddTrace(name = "login_data_repository_downloadEndpoints")
    fun downloadEndpoints() {
        endpointsDownloadUseCase.get({ statusData.setError(it) })
    }

    //endregion


    //region NetworkStatus

    @AddTrace(name = "login_data_repository_getErrors")
    fun getStatus(): LiveData<NetworkStatus> = statusData

    //endregion


    //region helper

    //endregion

}