package com.foodora.courier.aeolos.login.presentation

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import com.foodora.courier.aeolos.base.common.provider.CountryProvider
import com.foodora.courier.aeolos.base.common.provider.FirebaseEventProvider
import com.foodora.courier.aeolos.base.data.NetworkStatus
import com.foodora.courier.aeolos.base.data.StringLiveData
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.UserInterface.ENDPOINT_RESET
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.ViewModel.BRANDS_GET
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.ViewModel.BRAND_SELECT
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.ViewModel.COUNTRY_CODE_PRESET
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.ViewModel.COUNTRY_CODE_PRESET_FALSE
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.ViewModel.COUNTRY_CODE_PRESET_TRUE
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.ViewModel.COUNTRY_CODE_RESET
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.ViewModel.COUNTRY_CODE_SELECT
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.ViewModel.ENDPOINT_REFRESH
import com.foodora.courier.aeolos.login.data.LoginRepository
import com.foodora.courier.aeolos.login.domain.entities.Endpoint
import javax.inject.Inject

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

class LoginViewModel
@Inject
constructor(private val countryProvider: CountryProvider,
            private val firebaseEvent: FirebaseEventProvider,
            private val loginRepository: LoginRepository,
            internal val countryCode: StringLiveData) : ViewModel() {

    val countries: LiveData<List<String>>
    val email: ObservableField<String>
    val endpoint: LiveData<Endpoint>
    val password: ObservableField<String>
    val networkStatus: LiveData<NetworkStatus>

    init {
        countries = initCountries()
        email = ObservableField()
        endpoint = initEndpoint()
        password = ObservableField()
        networkStatus = initStatus()
    }


    //region brands

    fun getBrands(): List<Endpoint>? {
        val brands = loginRepository.getBrands(countryCode.value)
        firebaseEvent.log(BRANDS_GET, brands)
        return brands
    }

    fun selectBrand(brand: String) {
        firebaseEvent.log(BRAND_SELECT, brand)
        loginRepository.setEndpoint(countryCode.value!!, brand)
    }

    //endregion


    //region countries

    private fun initCountries(): LiveData<List<String>> {
        return loginRepository.getEndpoints()
    }

    //endregion


    //region countryCode

    fun presetCountryCode(): Boolean {
        val countryCode = countryProvider.getCountryCode()
        firebaseEvent.log(COUNTRY_CODE_PRESET, countryCode)

        return if (countries.value != null && countries.value!!.contains(countryCode)) {
            selectCountryCode(countryCode)
            firebaseEvent.log(COUNTRY_CODE_PRESET_TRUE)
            true
        } else {
            firebaseEvent.log(COUNTRY_CODE_PRESET_FALSE)
            false
        }
    }

    private fun resetCountryCode() {
        firebaseEvent.log(COUNTRY_CODE_RESET)
        countryCode.setString(null)
    }

    fun selectCountryCode(countryCode: String?) {
        firebaseEvent.log(COUNTRY_CODE_SELECT, countryCode)
        this.countryCode.setString(countryCode)
    }

    //endregion


    //region endpoints

    private fun initEndpoint(): LiveData<Endpoint> {
        return loginRepository.getEndpoint()
    }

    fun refreshEndpoints() {
        firebaseEvent.log(ENDPOINT_REFRESH)
        loginRepository.downloadEndpoints()
    }

    fun reset() {
        firebaseEvent.log(ENDPOINT_RESET)
        resetCountryCode()
        loginRepository.resetEndpoint()
    }

    //endregion


    //region networkStatus

    private fun initStatus(): LiveData<NetworkStatus> {
        return loginRepository.getStatus()
    }
    //endregion

}