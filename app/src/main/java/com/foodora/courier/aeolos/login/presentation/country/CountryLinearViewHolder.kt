package com.foodora.courier.aeolos.login.presentation.country

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.foodora.courier.aeolos.BR
import com.foodora.courier.aeolos.R
import com.foodora.courier.aeolos.base.presentation.recyclerview.DisplayableItem
import com.foodora.courier.aeolos.base.presentation.recyclerview.ViewHolderBinder
import com.foodora.courier.aeolos.base.presentation.recyclerview.ViewHolderFactory
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 22.10.17.
 */

internal class CountryLinearViewHolder
private constructor(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    private fun bind(country: CountryViewEntity) {
        binding.invalidateAll()
        binding.setVariable(BR.country, country)
        binding.executePendingBindings()
    }

    internal class Factory
    @Inject
    constructor(context: Context) : ViewHolderFactory(context) {

        @AddTrace(name = "login_country_createViewHolder_linear")
        override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding: ViewDataBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.viewholder_login_country_linear,
                    parent,
                    false
            )
            return CountryLinearViewHolder(binding)
        }
    }

    internal class Binder
    @Inject
    constructor() : ViewHolderBinder {
        override fun bind(viewHolder: RecyclerView.ViewHolder, item: DisplayableItem<Any>) {
            val castedViewHolder = CountryLinearViewHolder::class.java.cast(viewHolder)
            val viewEntity = CountryViewEntity::class.java.cast(item.model)
            castedViewHolder.bind(viewEntity)
        }
    }
}