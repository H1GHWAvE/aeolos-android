package com.foodora.courier.aeolos.login.injection.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.foodora.courier.aeolos.base.presentation.util.ViewModelUtil
import com.foodora.courier.aeolos.login.presentation.LoginViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

@Module
class LoginViewModelModule {

    @Singleton
    @Provides
    internal fun provideViewModelProviderFactory(viewModelUtil: ViewModelUtil,
                                                 viewModel: LoginViewModel):
            ViewModelProvider.Factory {

        return viewModelUtil.createFor<ViewModel>(viewModel)
    }
}
