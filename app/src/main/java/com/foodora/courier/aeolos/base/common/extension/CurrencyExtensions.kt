package com.foodora.courier.aeolos.base.common.extension

import com.google.firebase.perf.metrics.AddTrace
import java.text.NumberFormat
import java.util.*


/**
 * Created by johannes.neutze on 20.09.17.
 */

//region CurrencyExtensions

@AddTrace(name = "base_common_extension_currency_toCurrency")
fun Int.toCurrency(country: String): String {
    val locale = Locale(country.toLowerCase(), country.toUpperCase())
    return NumberFormat.getCurrencyInstance(locale).format(this.fromFractionCurrency())
}

//endregion


//region helper

@AddTrace(name = "base_common_extension_currency_toCurrency")
private fun Int.fromFractionCurrency(): Double {

    fun applyFraction(amount: Int): Double = amount.toDouble() / getFraction()

    return applyFraction(this)
}

@AddTrace(name = "base_common_extension_currency_toFractionCurrency")
private fun Double.toFractionCurrency(): Int {

    fun removeFraction(amount: Double): Int {
        val doubleAmount = amount * getFraction()
        return doubleAmount.toInt()
    }

    return removeFraction(this)
}

@AddTrace(name = "base_common_extension_currency_getFraction")
private fun getFraction(): Int {
    val currency = Currency.getInstance(Locale.getDefault())
    val fractionDigit = currency.defaultFractionDigits
    var fraction = 1

    if (fractionDigit > 0) {
        fraction = Math.pow(10.0, fractionDigit.toDouble()).toInt()
    }

    return fraction
}

//endregion