package com.foodora.courier.aeolos.base.common.extension

import com.google.firebase.perf.metrics.AddTrace
import java.util.*

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 22.10.17.
 */

@AddTrace(name = "base_common_extension_country_toCountryName")
fun String.toCountryName() : String = Locale("", this).getDisplayCountry(Locale.getDefault())