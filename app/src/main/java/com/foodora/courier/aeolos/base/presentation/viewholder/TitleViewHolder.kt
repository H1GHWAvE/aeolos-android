package com.foodora.courier.aeolos.base.presentation.viewholder

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.foodora.courier.aeolos.BR
import com.foodora.courier.aeolos.R
import com.foodora.courier.aeolos.base.presentation.recyclerview.DisplayableItem
import com.foodora.courier.aeolos.base.presentation.recyclerview.ViewHolderBinder
import com.foodora.courier.aeolos.base.presentation.recyclerview.ViewHolderFactory
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject


/**
 * Created by johannes.neutze on 20.09.17.
 */

internal class TitleViewHolder
private constructor(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    private fun bind(title: TitleViewEntity) {
        binding.invalidateAll()
        binding.setVariable(BR.title, title)
        binding.executePendingBindings()
    }

    internal class Factory
    @Inject
    constructor(context: Context) : ViewHolderFactory(context) {

        @AddTrace(name = "base_viewholder_title_createViewHolder")
        override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding: ViewDataBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.viewholder_base_title,
                    parent,
                    false
            )
            return TitleViewHolder(binding)
        }
    }

    internal class Binder
    @Inject
    constructor() : ViewHolderBinder {
        override fun bind(viewHolder: RecyclerView.ViewHolder, item: DisplayableItem<Any>) {
            val castedViewHolder = TitleViewHolder::class.java.cast(viewHolder)
            val viewEntity = TitleViewEntity::class.java.cast(item.model)
            castedViewHolder.bind(viewEntity)
        }
    }
}