package com.foodora.courier.aeolos.base.presentation.recyclerview

import android.support.v7.widget.RecyclerView.ViewHolder


/**
 * Created by johannes.neutze on 20.09.17.
 */

interface ViewHolderBinder {

    /**
     * Populates the passed [ViewHolder] with the details of the passed [DisplayableItem].
     */
    fun bind(viewHolder: ViewHolder, item: DisplayableItem<Any>)
}