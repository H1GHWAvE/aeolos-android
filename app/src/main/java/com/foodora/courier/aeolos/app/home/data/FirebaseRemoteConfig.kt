package com.foodora.courier.aeolos.app.home.data

import com.foodora.courier.aeolos.BuildConfig
import com.foodora.courier.aeolos.R
import com.google.firebase.perf.metrics.AddTrace
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import javax.inject.Inject


/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 23.10.17.
 */

class FirebaseRemoteConfig
@Inject
internal constructor() {

    private val remoteConfig = FirebaseRemoteConfig.getInstance()
    private var cacheExpiration: Long = 3600

    //region init

    @AddTrace(name = "home_data_remote")
    fun init() {
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build()

        remoteConfig.setConfigSettings(configSettings)
        remoteConfig.setDefaults(R.xml.firebase_remote_config_defaults)

        if (remoteConfig.info.configSettings.isDeveloperModeEnabled) {
            cacheExpiration = 0
        }

        remoteConfig.fetch(cacheExpiration).addOnCompleteListener { remoteConfig.activateFetched() }
    }

    //endregion


    //region login

    @AddTrace(name = "home_data_remote_getLoginRecyclerLayout")
    fun getLoginRecyclerLayout(): Boolean {
        val loginRecyclerLayout = "login_recycler_layout"
        val loginRecyclerLayoutValues = arrayOf("linear", "grid")

        return getString(loginRecyclerLayout) != loginRecyclerLayoutValues[1]
    }

    //endregion


    //region helper

    @AddTrace(name = "home_data_remote_boolean")
    private fun getBoolean(key: String): Boolean {
        return remoteConfig.getBoolean(key)
    }

    @AddTrace(name = "home_data_remote_double")
    private fun getDouble(key: String): Double {
        return remoteConfig.getDouble(key)
    }

    @AddTrace(name = "home_data_remote_string")
    private fun getString(key: String): String {
        return remoteConfig.getString(key)
    }

    //endregion

}