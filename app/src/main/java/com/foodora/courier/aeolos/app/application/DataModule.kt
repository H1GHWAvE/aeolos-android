package com.foodora.courier.aeolos.app.application

import com.foodora.courier.aeolos.app.home.data.SessionRepository
import com.foodora.courier.aeolos.login.injection.module.LoginDataModule
import com.foodora.courier.base.domain.db.SessionDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 04.10.17.
 */

@Module(includes = arrayOf(LoginDataModule::class))
internal class DataModule {

    @Singleton
    @Provides
    fun provideSessionRepository(sessionDao: SessionDao): SessionRepository {

        return SessionRepository(sessionDao)
    }

}