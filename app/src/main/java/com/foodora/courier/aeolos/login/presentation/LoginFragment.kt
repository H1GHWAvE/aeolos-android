package com.foodora.courier.aeolos.login.presentation

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.foodora.courier.aeolos.R
import com.foodora.courier.aeolos.base.presentation.BaseInjectingActivity
import com.foodora.courier.aeolos.base.presentation.BaseInjectingFragment
import com.foodora.courier.aeolos.databinding.FragmentLoginBinding
import com.foodora.courier.aeolos.login.injection.component.LoginFragmentComponent
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject


/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

class LoginFragment : BaseInjectingFragment() {


    @Inject lateinit internal var viewModelFactory: ViewModelProvider.Factory
    @Inject lateinit internal var decorator: LoginDecorator
    @Inject lateinit internal var presenter: LoginPresenter

    lateinit private var binding: FragmentLoginBinding
    lateinit private var viewModel: LoginViewModel


    //region lifecycle

    @AddTrace(name = "login_fragment_onCreateView")
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)

        decorator.bind(binding)
        presenter.initialize(decorator, viewModel)

        return binding.root
    }

    @AddTrace(name = "login_fragment_onDestroy")
    override fun onDestroy() {
        super.onDestroy()
        presenter.dispose()
        decorator.dispose()
    }

    //endregion


    //region extend

    @AddTrace(name = "login_fragment_onInject")
    override fun onInject() {
        val activity = BaseInjectingActivity::class.java.cast(activity)
        val componentCreator = LoginFragmentComponent.LoginComponentCreator::class.java.cast(activity.getComponent())
        componentCreator.createLoginFragmentComponent().inject(this)
    }

    //endregion

}