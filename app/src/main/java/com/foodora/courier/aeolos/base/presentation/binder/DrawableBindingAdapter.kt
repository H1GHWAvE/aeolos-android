package com.foodora.courier.aeolos.base.presentation.binder

import android.databinding.BindingAdapter
import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.google.firebase.perf.metrics.AddTrace

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 09.10.17.
 */

object DrawableBindingAdapter {

    @AddTrace(name = "base_binder_drawable_setImageResource")
    @BindingAdapter("android:src")
    @JvmStatic
    fun setImageResource(imageView: ImageView, @DrawableRes resource: Int) {
        imageView.setImageResource(resource)
    }

}