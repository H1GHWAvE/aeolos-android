package com.foodora.courier.aeolos.login.presentation.brand

import com.foodora.courier.aeolos.login.presentation.LoginUserInterface

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 22.10.17.
 */

data class BrandViewEntity(val delegate: LoginUserInterface.Delegate,
                           val icon: Int,
                           val name: String)
