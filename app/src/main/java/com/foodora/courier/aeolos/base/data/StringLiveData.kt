package com.foodora.courier.aeolos.base.data

import android.arch.lifecycle.LiveData
import javax.inject.Inject

/**
 * Created by johannes.neutze on 21.09.17.
 */

class StringLiveData
@Inject
constructor(private val executor: MainThreadExecutor) : LiveData<String>() {

    fun setString(value: String?) {
        executor.execute {
            super.setValue(value)
        }
    }
}