package com.foodora.courier.aeolos.base.network.interceptor

import com.foodora.courier.aeolos.BuildConfig
import com.google.firebase.perf.metrics.AddTrace
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by johannes.neutze on 28.09.17.
 */

class UserAgentInterceptor : Interceptor {

    @AddTrace(name = "base_network_interceptor_user")
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val requestBuilder = original.newBuilder().header("User-Agent", "Aeolos/" + BuildConfig.VERSION_NAME)

        requestBuilder.method(original.method(), original.body())

        return chain.proceed(requestBuilder.build())
    }

}