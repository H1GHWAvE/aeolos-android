package com.foodora.courier.aeolos.base.common.provider

import android.content.Context.TELEPHONY_SERVICE
import android.telephony.TelephonyManager
import com.foodora.courier.aeolos.app.application.AeolosApplication
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject


/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 22.10.17.
 */

class CountryProvider
@Inject
internal constructor(private val app: AeolosApplication) {

    @AddTrace(name = "base_common_provider_country")
    fun getCountryCode(): String? {
        val telephonyManager = app.getSystemService(TELEPHONY_SERVICE) as TelephonyManager
        return telephonyManager.networkCountryIso
    }

}