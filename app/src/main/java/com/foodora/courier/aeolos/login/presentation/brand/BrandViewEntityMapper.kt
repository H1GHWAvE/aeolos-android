package com.foodora.courier.aeolos.login.presentation.brand

import com.foodora.courier.aeolos.base.common.extension.toLogoDrawable
import com.foodora.courier.aeolos.base.common.mapper.Mapper
import com.foodora.courier.aeolos.login.domain.entities.Endpoint
import com.foodora.courier.aeolos.login.presentation.LoginUserInterface
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

class BrandViewEntityMapper
@Inject
constructor() : Mapper<List<BrandViewEntity>, List<Endpoint>> {

    private lateinit var delegate: LoginUserInterface.Delegate

    @AddTrace(name = "login_brand_map")
    override fun map(input: List<Endpoint>): List<BrandViewEntity> {
        val countries = mutableListOf<BrandViewEntity>()

        return input.mapTo(countries) {
            BrandViewEntity(
                    delegate = delegate,
                    icon = it.brand.toLogoDrawable(),
                    name = it.brand
            )
        }
    }

    fun map(input: List<Endpoint>, delegate: LoginUserInterface.Delegate): List<BrandViewEntity> {
        this.delegate = delegate

        return map(input)
    }
}