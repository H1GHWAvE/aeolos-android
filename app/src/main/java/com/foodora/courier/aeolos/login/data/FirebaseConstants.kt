package com.foodora.courier.aeolos.login.data

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 23.10.17.
 */

object FirebaseConstants {

    object Event {

        object Decorator {
            const val LAYOUT_GRID = "login_layout_grid"
            const val LAYOUT_LINEAR = "login_layout_linear"
            const val OBSERVER_COUNTRIES = "login_decorator_observer_countries"
            const val OBSERVER_COUNTRY_CODE = "login_decorator_observer_countryCode"
            const val OBSERVER_CREDENTIALS = "login_decorator_observer_credentials"
            const val OBSERVER_ENDPOINT = "login_decorator_observer_endpoint"
            const val OBSERVER_STATUS = "login_decorator_observer_status"
            const val REFRESH = "login_decorator_refresh"
        }

        object UseCase {
            const val ENDPOINTS_DOWNLOAD = "login_network_endpoints_download"
            const val ENDPOINTS_DOWNLOAD_ERROR = "login_network_endpoints_download_error"
            const val ENDPOINTS_DOWNLOAD_SUCCESS = "login_network_endpoints_download_success"
        }

        object UserInterface {
            const val BRAND_SELECT = "login_user_onBrandSelect"
            const val COUNTRY_RESET = "login_user_onCountryReset"
            const val COUNTRY_SELECT = "login_user_onCountrySelect"
            const val ENDPOINT_RESET = "login_user_onEndpointReset"
            const val LOGIN = "login_user_onLogin"
            const val REFRESH = "login_user_onRefresh"
        }

        object ViewModel {
            const val BRAND_SELECT = "login_view_selectBrand"
            const val BRANDS_GET = "login_view_getBrands"
            const val COUNTRY_CODE_PRESET = "login_view_initCountryCode"
            const val COUNTRY_CODE_PRESET_FALSE = "login_view_initCountryCode_false"
            const val COUNTRY_CODE_PRESET_TRUE = "login_view_initCountryCode_true"
            const val COUNTRY_CODE_RESET = "login_view_resetCountryCode"
            const val COUNTRY_CODE_SELECT = "login_view_selectCountryCode"
            const val ENDPOINT_REFRESH = "login_view_refreshEndpoint"
        }
    }

}