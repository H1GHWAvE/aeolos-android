package com.foodora.courier.aeolos.base.data

import android.arch.lifecycle.LiveData
import javax.inject.Inject

/**
 * Created by johannes.neutze on 21.09.17.
 */

class NetworkStatusLiveData
@Inject
constructor(private val executor: MainThreadExecutor) : LiveData<NetworkStatus>() {

    fun setError(value: NetworkStatus) {
        executor.execute {
            super.setValue(value)
        }
    }
}