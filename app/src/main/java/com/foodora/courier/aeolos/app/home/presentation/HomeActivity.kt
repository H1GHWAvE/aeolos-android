package com.foodora.courier.aeolos.app.home.presentation

import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.foodora.courier.aeolos.R
import com.foodora.courier.aeolos.app.application.AeolosApplication
import com.foodora.courier.aeolos.base.injection.module.ActivityModule
import com.foodora.courier.aeolos.base.presentation.BaseInjectingActivity
import com.foodora.courier.aeolos.login.presentation.LoginFragment
import com.google.firebase.perf.metrics.AddTrace


/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 04.10.17.
 */

class HomeActivity : BaseInjectingActivity<HomeActivityComponent>() {


    //region lifecycle

    @AddTrace(name = "home_activity_onCreate")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        showLoginFragment()
    }

    //endregion


    //region extend

    @AddTrace(name = "home_activity_onInject")
    override fun onInject(component: HomeActivityComponent) {
        createComponent().inject(this)
    }

    @AddTrace(name = "home_activity_createComponent")
    override fun createComponent(): HomeActivityComponent {
        val app = AeolosApplication::class.java.cast(application)
        val activityModule = ActivityModule(this)
        return app.component!!.createHomeActivityComponent(activityModule)
    }

    override val activity: Activity
        get() = this
    override val context: Context
        get() = this

    //endregion


    //region fragments

    @AddTrace(name = "home_activity_showLoginFragment")
    private fun showLoginFragment() {
        supportFragmentManager.beginTransaction()
                .add(R.id.frame_home_fragment, LoginFragment())
                .commitNow()
    }

    //endregion

}