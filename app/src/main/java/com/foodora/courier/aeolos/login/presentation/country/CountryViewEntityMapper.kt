package com.foodora.courier.aeolos.login.presentation.country

import com.foodora.courier.aeolos.base.common.extension.toCountryName
import com.foodora.courier.aeolos.base.common.extension.toFlagDrawable
import com.foodora.courier.aeolos.base.common.mapper.Mapper
import com.foodora.courier.aeolos.login.presentation.LoginUserInterface
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

class CountryViewEntityMapper
@Inject
constructor() : Mapper<List<CountryViewEntity>, List<String>> {

    private lateinit var delegate: LoginUserInterface.Delegate

    @AddTrace(name = "login_country_map")
    override fun map(input: List<String>): List<CountryViewEntity> {
        val countries = mutableListOf<CountryViewEntity>()

        return input.mapTo(countries) {
            CountryViewEntity(
                    country = it.toCountryName(),
                    countryCode = it,
                    delegate = delegate,
                    flag = it.toFlagDrawable()
            )
        }.sortedBy { it.country }
    }

    fun map(input: List<String>, delegate: LoginUserInterface.Delegate): List<CountryViewEntity> {
        this.delegate = delegate

        return map(input)
    }
}