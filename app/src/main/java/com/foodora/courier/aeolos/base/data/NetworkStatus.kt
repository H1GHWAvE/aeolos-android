package com.foodora.courier.aeolos.base.data

/**
 * Created by johannes.neutze on 21.09.17.
 */

enum class NetworkStatus {
    DISCONNECTED,
    FORBIDDEN,
    HTTP,
    LOADING,
    NOT_A_FEED,
    PARSE,
    SUCCESS,
    UNAUTHORIZED,
    UNKNOWN,
}