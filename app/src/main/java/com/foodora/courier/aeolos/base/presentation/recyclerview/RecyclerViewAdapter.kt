package com.foodora.courier.aeolos.base.presentation.recyclerview

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup
import com.foodora.courier.aeolos.base.common.precondition.AndroidPreconditions
import com.google.firebase.perf.metrics.AddTrace


/**
 * Created by johannes.neutze on 20.09.17.
 */

class RecyclerViewAdapter(private val comparator: ItemComparator,
                          private val factoryMap: Map<Int, ViewHolderFactory>,
                          private val binderMap: Map<Int, ViewHolderBinder>,
                          private val androidPreconditions: AndroidPreconditions) : RecyclerView.Adapter<ViewHolder>() {

    private val modelItems = arrayListOf<DisplayableItem<Any>>()

    @AddTrace(name = "base_recycler_onCreateViewHolder")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder? {
        //TODO (JN): ? instead of !!
        return factoryMap[viewType]!!.createViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = modelItems[position]
        //TODO (JN): ? instead of !!
        binderMap[item.type]!!.bind(holder, item)
    }

    @AddTrace(name = "base_recycler_getItemCount")
    override fun getItemCount(): Int {
        return modelItems.size
    }

    @AddTrace(name = "base_recycler_getItemViewType")
    override fun getItemViewType(position: Int): Int {
        return modelItems[position].type
    }

    /**
     * Updates modelItems currently stored in adapter with the new modelItems.

     * @param items collection to update the previous values
     */
    fun update(items: List<DisplayableItem<Any>>) {
        androidPreconditions.assertUiThread()

        if (modelItems.isEmpty()) {
            updateAllItems(items)
        } else {
            //TODO (JN) unknown functionality updateDiffItemsOnly(items)
            updateAllItems(items)
        }
    }

    /**
     * Only use for the first update of the adapter, whe it is still empty.
     */
    private fun updateAllItems(items: List<DisplayableItem<Any>>) {
        updateItemsInModel(items)
        notifyDataSetChanged()
    }

    /**
     * Do not use for first update of the adapter. The method [DiffUtil.DiffResult.dispatchUpdatesTo] is significantly slower than [ ][RecyclerViewAdapter.notifyDataSetChanged] when it comes to update all the items in the adapter.
     */
    private fun updateDiffItemsOnly(items: List<DisplayableItem<Any>>) {
        updateAdapterWithDiffResult(calculateDiff(items))
        notifyDataSetChanged()
    }

    @AddTrace(name = "base_recycler_calculateDiff")
    private fun calculateDiff(newItems: List<DisplayableItem<Any>>): DiffUtil.DiffResult {
        return DiffUtil.calculateDiff(DiffUtilCallback(modelItems, newItems, comparator))
    }

    private fun updateItemsInModel(items: List<DisplayableItem<Any>>) {
        modelItems.clear()
        modelItems.addAll(items)
    }

    private fun updateAdapterWithDiffResult(result: DiffUtil.DiffResult) {
        result.dispatchUpdatesTo(this)
    }

    @AddTrace(name = "base_recycler_getItemAtPosition")
    fun getItemAtPosition(position: Int): DisplayableItem<Any> {
        return modelItems[position]
    }
}