package com.foodora.courier.aeolos.base.network.interceptor

import com.google.firebase.perf.metrics.AddTrace
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by johannes.neutze on 28.09.17.
 */

class AcceptDialogInterceptor : Interceptor {
    private val headerName = "AcceptDialog"
    private val headerValue = "application/json"

    @AddTrace(name = "base_network_interceptor_accept")
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val requestBuilder = original.newBuilder().header(headerName, headerValue)

        requestBuilder.method(original.method(), original.body())

        return chain.proceed(requestBuilder.build())
    }

}