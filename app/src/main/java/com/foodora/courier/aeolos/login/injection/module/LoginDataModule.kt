package com.foodora.courier.aeolos.login.injection.module

import com.foodora.courier.aeolos.base.data.NetworkStatusLiveData
import com.foodora.courier.aeolos.login.data.EndpointsDownloadUseCase
import com.foodora.courier.aeolos.login.data.LoginRepository
import com.foodora.courier.aeolos.login.domain.db.EndpointDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

@Module
internal class LoginDataModule {

    @Singleton
    @Provides
    fun provideEndpointRepository(endpointDao: EndpointDao,
                                  endpointsDownloadUseCase: EndpointsDownloadUseCase,
                                  statusLiveData: NetworkStatusLiveData): LoginRepository {

        return LoginRepository(
                endpointDao,
                endpointsDownloadUseCase,
                statusLiveData
        )
    }

}