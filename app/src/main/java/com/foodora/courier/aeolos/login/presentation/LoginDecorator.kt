package com.foodora.courier.aeolos.login.presentation

import android.arch.lifecycle.Observer
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ProgressBar
import butterknife.BindView
import butterknife.ButterKnife
import com.foodora.courier.aeolos.R
import com.foodora.courier.aeolos.app.home.data.FirebaseRemoteConfig
import com.foodora.courier.aeolos.base.common.extension.convertToPixel
import com.foodora.courier.aeolos.base.common.provider.FirebaseEventProvider
import com.foodora.courier.aeolos.base.data.NetworkStatus
import com.foodora.courier.aeolos.base.presentation.BaseActivity
import com.foodora.courier.aeolos.base.presentation.recyclerview.RecyclerViewAdapter
import com.foodora.courier.aeolos.databinding.FragmentLoginBinding
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.Decorator.LAYOUT_GRID
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.Decorator.LAYOUT_LINEAR
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.Decorator.OBSERVER_COUNTRIES
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.Decorator.OBSERVER_COUNTRY_CODE
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.Decorator.OBSERVER_CREDENTIALS
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.Decorator.OBSERVER_ENDPOINT
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.Decorator.OBSERVER_STATUS
import com.foodora.courier.aeolos.login.data.FirebaseConstants.Event.Decorator.REFRESH
import com.foodora.courier.aeolos.login.domain.entities.Endpoint
import com.foodora.courier.aeolos.login.presentation.brand.BrandDisplayableItemMapper
import com.foodora.courier.aeolos.login.presentation.country.CountryDisplayableItemMapper
import com.foodora.courier.aeolos.login.presentation.endpoint.EndpointViewEntityMapper
import com.google.firebase.perf.metrics.AddTrace
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

class LoginDecorator
@Inject
constructor(private val activity: BaseActivity,
            private val brandDisplayableItemMapper: BrandDisplayableItemMapper,
            private val countryDisplayableItemMapper: CountryDisplayableItemMapper,
            private val endpointViewEntityMapper: EndpointViewEntityMapper,
            private val firebaseEvent: FirebaseEventProvider,
            private val recyclerAdapter: RecyclerViewAdapter,
            firebaseRemoteConfig: FirebaseRemoteConfig) : LoginUserInterface {

    //region ButterKnife

    @BindView(R.id.recycler_fragment_login_country)
    lateinit var countryRecyclerView: RecyclerView
    @BindView(R.id.recycler_fragment_login_brand)
    lateinit var brandRecyclerView: RecyclerView
    @BindView(R.id.progressbar_login)
    lateinit var progressBar: ProgressBar

    //endregion


    //region var

    lateinit private var binding: FragmentLoginBinding
    lateinit private var viewModel: LoginViewModel
    private var delegate: LoginUserInterface.Delegate? = null
    private var linearLayout = firebaseRemoteConfig.getLoginRecyclerLayout()
    private var preset = true

    //endregion


    //region init

    @AddTrace(name = "login_decorator_bind")
    fun bind(binding: FragmentLoginBinding) {
        ButterKnife.bind(this, binding.root)
        this.binding = binding

        configureCountryRecyclerView()
        configureBrandRecyclerView()
        configureProgressBar()
    }

    @AddTrace(name = "login_decorator_initialize")
    override fun initialize(delegate: LoginUserInterface.Delegate,
                            viewModel: LoginViewModel) {

        this.delegate = delegate
        this.viewModel = viewModel

        binding.delegate = delegate
        binding.email = viewModel.email
        binding.password = viewModel.password

        setCountriesObserver()
        setCountryCodeObserver()
        setEndpointObserver()
        setCredentialsObserver()
        setStatusObserver()
    }

    @AddTrace(name = "login_decorator_dispose")
    fun dispose() {
        delegate = null
    }

    //endregion


    //region observer

    private fun setCountriesObserver() {
        viewModel.countries.observe(activity, Observer {
            firebaseEvent.log(OBSERVER_COUNTRIES, it)
            setCountries(it!!)
        })
    }

    private fun setCountryCodeObserver() {
        viewModel.countryCode.observe(activity, Observer {
            firebaseEvent.log(OBSERVER_COUNTRY_CODE, it)
            setBrands(it)
        })
    }

    private fun setCredentialsObserver() {
        firebaseEvent.log(OBSERVER_CREDENTIALS)
    }

    private fun setEndpointObserver() {
        viewModel.endpoint.observe(activity, Observer {
            firebaseEvent.log(OBSERVER_ENDPOINT, it)
            setEndpoint(it)
        })
    }

    private fun setStatusObserver() {
        viewModel.networkStatus.observe(activity, Observer {
            firebaseEvent.log(OBSERVER_STATUS, it)
            setStatus(it)
        })
    }

    //endregion


    //region brands

    private fun configureBrandRecyclerView() {
        brandRecyclerView.adapter = recyclerAdapter

        brandRecyclerView.layoutManager = getLayoutManager()
        brandRecyclerView.setHasFixedSize(true)
    }

    private fun presetBrands(countryCode: String?) {
        if (!countryCode.isNullOrBlank()) {
            val brands = viewModel.getBrands()
            if (brands?.size == 1) {
                //TODO (JN): !! replace
                delegate!!.onBrandSelect(brand = brands[0].brand)
            } else
                updateBrandsAdapter(brands)
        }
    }

    private fun setBrands(countryCode: String?) {
        setCountryCode(countryCode)
        presetBrands(countryCode)
    }

    private fun setCountryCode(countryCode: String?) {
        binding.countryCode = countryCode
    }

    private fun updateBrandsAdapter(brands: List<Endpoint>?) {
        //TODO (JN): !! replace
        recyclerAdapter.update(brandDisplayableItemMapper.map(brands!!, delegate!!, linearLayout))
    }

    //endregion


    //region countries

    private fun configureCountryRecyclerView() {
        countryRecyclerView.adapter = recyclerAdapter
        countryRecyclerView.layoutManager = getLayoutManager()
        countryRecyclerView.setHasFixedSize(true)
    }

    private fun presetCountry() {
        if (preset) {
            if (viewModel.presetCountryCode()) {
                preset = false
            }
        }
    }

    private fun setCountries(countries: List<String>) {
        binding.endpoints = countries

        Timber.e("%s", countries)
        if (countries.isNotEmpty()) {
            if (viewModel.countryCode.value == null && viewModel.endpoint.value == null) {
                updateCountriesRecycler(countries)
                presetCountry()
            }
        } else {
            updateCountriesRecycler(listOf())
            refresh()
        }
    }

    private fun updateCountriesRecycler(countries: List<String>) {
        //TODO (JN): !! replace
        recyclerAdapter.update(countryDisplayableItemMapper.map(
                countries,
                delegate!!,
                linearLayout
        ))
    }

    //endregion


    //region endpoint

    private fun setEndpoint(endpoint: Endpoint?) {
        if (viewModel.countryCode.value == null) {
            viewModel.selectCountryCode(endpoint?.countryCode)
            preset = false
        }

        binding.endpoint = endpointViewEntityMapper.map(endpoint)
    }

    //endregion


    //region progressBar

    private fun configureProgressBar() {
        Timber.e("%s", getStatusBarHeight())
       progressBar.setPadding(0, getStatusBarHeight(), 0, 0)
    }

    //endregion


    //region status

    private fun setStatus(networkStatus: NetworkStatus?) {
        binding.status = networkStatus
    }

    //endregion


    //region helper

    private fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = activity.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = activity.resources.getDimensionPixelSize(resourceId)
        }
        return result - 7.convertToPixel(activity)
    }

    private fun refresh() {
        viewModel.refreshEndpoints()
        firebaseEvent.log(REFRESH)
    }

    private fun getLayoutManager(): RecyclerView.LayoutManager {
        return if (linearLayout) {
            firebaseEvent.log(LAYOUT_LINEAR)
            LinearLayoutManager(activity)
        } else {
            firebaseEvent.log(LAYOUT_GRID)
            GridLayoutManager(activity, 4, GridLayoutManager.VERTICAL, false)
        }
    }

    //endregion

}