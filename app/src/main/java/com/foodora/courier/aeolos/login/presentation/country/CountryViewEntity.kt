package com.foodora.courier.aeolos.login.presentation.country

import com.foodora.courier.aeolos.login.presentation.LoginUserInterface

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 22.10.17.
 */

data class CountryViewEntity(val country: String,
                             val countryCode: String,
                             val delegate: LoginUserInterface.Delegate,
                             val flag: Int)
