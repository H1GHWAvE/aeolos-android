package com.foodora.courier.base.domain.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 03.10.17.
 */

@Entity(tableName = "session")
data class Session(@PrimaryKey @ColumnInfo(name = "id_session") var id: Long = 14490,
                   val token: String?,
                   val countryUrl: String?,
                   val country: String?)