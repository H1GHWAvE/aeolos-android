package com.foodora.courier.aeolos.base.common.extension

import javax.inject.Inject


/**
 * Created by johannes.neutze on 20.09.17.
 */

class StringUtils
@Inject
internal constructor() {

    fun applySubstitutionsToString(string: String, vararg substitutions: Pair<String, String>): String {
        var primaryString = string

        for ((first, second) in substitutions) {
            primaryString = primaryString.replace("{{$first}}", second)
        }
        return primaryString
    }

}