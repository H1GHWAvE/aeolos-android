package com.foodora.courier.aeolos.login.network.model

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 07.08.17.
 */

data class EndpointsResponse(var endpoints: ArrayList<BrandResponse>)