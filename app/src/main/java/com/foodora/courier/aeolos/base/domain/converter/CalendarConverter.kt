package com.foodora.courier.aeolos.base.domain.converter

import android.arch.persistence.room.TypeConverter
import com.google.firebase.perf.metrics.AddTrace
import java.util.*

/**
 * Created by johannes.neutze on 20.09.17.
 */

class CalendarConverter {

    @AddTrace(name = "base_domain_converter_calendar_toCalendar")
    @TypeConverter
    fun toCalendar(timestamp: Long): Calendar {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = timestamp
        return calendar
    }

    @AddTrace(name = "base_domain_converter_calendar_toTimestamp")
    @TypeConverter
    fun toTimestamp(calendar: Calendar): Long = calendar.timeInMillis

}