package com.foodora.courier.aeolos.base.presentation.viewholder

import android.content.Context
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 06.10.17.
 */

class SpacerViewEntityFactory
@Inject
constructor(val context: Context) {

    @AddTrace(name = "base_spacer_create")
    fun <T> create(clazz: T): TitleViewEntity {
        return when (clazz) {   else -> throw Exception()
        }
    }

}