package com.foodora.courier.base.domain.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.foodora.courier.base.domain.entity.Session

/**
 * Created by johannes.neutze on 26.09.17.
 */

@Dao
abstract class SessionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(session: Session) : Long

    @Query("SELECT * FROM session WHERE id_session = 14490")
    abstract fun getSession(): Session

}