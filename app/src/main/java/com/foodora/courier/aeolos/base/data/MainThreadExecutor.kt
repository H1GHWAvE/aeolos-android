package com.foodora.courier.aeolos.base.data

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import javax.inject.Inject

/**
 * Created by johannes.neutze on 21.09.17.
 */

class MainThreadExecutor
@Inject
constructor() : Executor {

    private val mainThreadHandler = Handler(Looper.getMainLooper())

    override fun execute(command: Runnable) {
        mainThreadHandler.post(command)
    }
}