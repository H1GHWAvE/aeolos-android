package com.foodora.courier.aeolos.login.presentation

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

interface LoginUserInterface {

    fun initialize(delegate: Delegate,
                   viewModel: LoginViewModel)

    interface Delegate {

        fun onBrandSelect(brand: String)

        fun onCountrySelect(countryCode: String)

        fun onCountryReset()

        fun onEndpointReset()

        fun onLogin()

        fun onRefresh()

    }
}