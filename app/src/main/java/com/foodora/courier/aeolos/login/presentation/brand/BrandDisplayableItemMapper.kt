package com.foodora.courier.aeolos.login.presentation.brand

import com.foodora.courier.aeolos.base.presentation.recyclerview.DisplayableItem
import com.foodora.courier.aeolos.login.domain.entities.Endpoint
import com.foodora.courier.aeolos.login.presentation.LoginUserInterface
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

class BrandDisplayableItemMapper
@Inject
constructor(private val brandGridDisplayableItemMapper: BrandGridDisplayableItemMapper,
            private val brandLinearDisplayableItemMapper: BrandLinearDisplayableItemMapper,
            private val brandViewEntityMapper: BrandViewEntityMapper) {

    @AddTrace(name = "login_brand_display_map")
    fun map(input: List<Endpoint>,
            delegate: LoginUserInterface.Delegate,
            linearLayout: Boolean): List<DisplayableItem<Any>> {

        return brandViewEntityMapper.map(input, delegate).let {
            if (linearLayout) {
                brandLinearDisplayableItemMapper.map(it)
            } else {
                brandGridDisplayableItemMapper.map(it)
            }
        }
    }

}