package com.foodora.courier.aeolos.login.presentation

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

object LoginConstants {

    object DisplayableTypes {
        const val BRAND_GRID = 0
        const val BRAND_LINEAR = 1
        const val COUNTRY_GRID = 2
        const val COUNTRY_LINEAR = 3
        const val SPACER = 4
    }
}