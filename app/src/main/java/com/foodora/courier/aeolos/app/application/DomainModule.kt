package com.foodora.courier.aeolos.app.application

import android.app.Application
import android.arch.persistence.room.Room
import com.foodora.courier.aeolos.app.home.domain.db.Database
import com.foodora.courier.aeolos.login.domain.db.EndpointDao
import com.foodora.courier.base.domain.db.SessionDao
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Singleton

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.09.17.
 */

@Module
class DomainModule {

    @Singleton
    @Provides
    fun provideDb(application: Application): Database = Room.databaseBuilder(application, Database::class.java, "roadrunner.db")
            .allowMainThreadQueries()
            .build()

    @Singleton
    @Provides
    fun provideCountriesDao(db: Database): EndpointDao = db.countriesDao()

    @Singleton
    @Provides
    fun provideSessionDao(db: Database): SessionDao = db.sessionDao()

    @Singleton
    @Provides
    fun provideExecutor(): Executor = Executors.newSingleThreadExecutor()

}
