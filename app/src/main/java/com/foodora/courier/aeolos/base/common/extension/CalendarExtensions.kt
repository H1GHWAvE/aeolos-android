package com.foodora.courier.aeolos.base.common.extension

import com.google.firebase.perf.metrics.AddTrace
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by johannes.neutze on 20.09.17.
 */

//region CalendarExtensions

@AddTrace(name = "base_common_extension_calendar_toCalendar")
fun String.toCalendar(): Calendar {
    val format = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

    val formatter = SimpleDateFormat(format, Locale.getDefault())
    val parsedDate = formatter.parse(this)
    val result = Calendar.getInstance()

    result.time = parsedDate

    return result
}

@AddTrace(name = "base_common_extension_calendar_toDate")
fun Calendar.toDate(): String {
    val malaysia = Locale.getDefault()
    val df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, malaysia)
    return df.format(this.time)
}

//endregion