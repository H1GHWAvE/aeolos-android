package com.foodora.courier.aeolos.base.common.alias

/**
* Created by Johannes Neutze
* johannes (at) foodora.com
* on 14.09.17.
*/

typealias Callback<T> = (T) -> Unit