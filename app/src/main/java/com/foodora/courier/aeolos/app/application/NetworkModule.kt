package com.foodora.courier.aeolos.app.application

import android.app.Application
import com.foodora.courier.aeolos.BuildConfig
import com.foodora.courier.aeolos.base.network.interceptor.AcceptDialogInterceptor
import com.foodora.courier.aeolos.base.network.interceptor.HttpErrorInterceptor
import com.foodora.courier.aeolos.base.network.interceptor.UserAgentInterceptor
import com.foodora.courier.aeolos.mock.MockInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor


/**
 * Created by johannes.neutze on 21.09.17.
 */

@Module
class NetworkModule {

    @Provides
    fun provideAcceptDialogInterceptor(): AcceptDialogInterceptor {
        return AcceptDialogInterceptor()
    }

    @Provides
    fun provideUserAgentInterceptor(): UserAgentInterceptor {
        return UserAgentInterceptor()
    }

    @Provides
    fun provideHttpErrorInterceptor(): HttpErrorInterceptor {
        return HttpErrorInterceptor()
    }

    @Provides
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return interceptor
    }

    @Provides
    fun provideMockInterceptor(app: Application): MockInterceptor {
        return MockInterceptor(200, app)
    }

    @Provides
    fun provideOkHttpClient(acceptDialogInterceptor: AcceptDialogInterceptor,
                            userAgentInterceptor: UserAgentInterceptor,
                            mockInterceptor: MockInterceptor,
                            httpErrorInterceptor: HttpErrorInterceptor,
                            httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        val client = OkHttpClient.Builder()
                .addInterceptor(acceptDialogInterceptor)
                .addInterceptor(userAgentInterceptor)
                .addInterceptor(httpErrorInterceptor)
                .addInterceptor(httpLoggingInterceptor)

        if (BuildConfig.BUILD_TYPE == "debug")
            client.addInterceptor(mockInterceptor)

        //.addNetworkInterceptor(StethoInterceptor())
        return client.build()
    }
}