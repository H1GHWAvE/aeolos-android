package com.foodora.courier.aeolos.base.common.mapper

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 07.08.17.
 */

interface Mapper<out OUT, in IN> {
    fun map(input: IN): OUT
}