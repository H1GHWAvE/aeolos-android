package com.foodora.courier.aeolos.login.presentation.country

import com.foodora.courier.aeolos.base.presentation.recyclerview.DisplayableItem
import com.foodora.courier.aeolos.login.presentation.LoginUserInterface
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

class CountryDisplayableItemMapper
@Inject
constructor(private val countryGridDisplayableItemMapper: CountryGridDisplayableItemMapper,
            private val countryLinearDisplayableItemMapper: CountryLinearDisplayableItemMapper,
            private val countryViewEntityMapper: CountryViewEntityMapper) {

    @AddTrace(name = "login_country_display_map")
    fun map(input: List<String>,
            delegate: LoginUserInterface.Delegate,
            linearLayout: Boolean): List<DisplayableItem<Any>> {

        return countryViewEntityMapper.map(input, delegate).let {
            if (linearLayout) {
                countryLinearDisplayableItemMapper.map(it)
            } else {
                countryGridDisplayableItemMapper.map(it)
            }
        }
    }

}