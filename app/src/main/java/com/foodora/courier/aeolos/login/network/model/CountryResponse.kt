package com.foodora.courier.aeolos.login.network.model

import com.google.gson.annotations.SerializedName

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 07.08.17.
 */

data class CountryResponse(@SerializedName("country_code") var countryCode: String,
                           var url: String)