package com.foodora.courier.aeolos.login.injection.module;

import com.foodora.courier.aeolos.base.common.precondition.AndroidPreconditions;
import com.foodora.courier.aeolos.base.presentation.recyclerview.ItemComparator;
import com.foodora.courier.aeolos.base.presentation.recyclerview.RecyclerViewAdapter;
import com.foodora.courier.aeolos.base.presentation.recyclerview.ViewHolderBinder;
import com.foodora.courier.aeolos.base.presentation.recyclerview.ViewHolderFactory;
import com.foodora.courier.aeolos.base.presentation.viewholder.SpacerViewHolder;
import com.foodora.courier.aeolos.login.presentation.LoginConstants;
import com.foodora.courier.aeolos.login.presentation.LoginItemComparator;
import com.foodora.courier.aeolos.login.presentation.brand.BrandGridViewHolder;
import com.foodora.courier.aeolos.login.presentation.brand.BrandLinearViewHolder;
import com.foodora.courier.aeolos.login.presentation.country.CountryGridViewHolder;
import com.foodora.courier.aeolos.login.presentation.country.CountryLinearViewHolder;

import java.util.Map;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntKey;
import dagger.multibindings.IntoMap;

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

@Module
public abstract class LoginFragmentModule {

    @Provides
    static RecyclerViewAdapter provideRecyclerAdapter(ItemComparator itemComparator,
                                                      Map<Integer, ViewHolderFactory> factoryMap,
                                                      Map<Integer, ViewHolderBinder> binderMap,
                                                      AndroidPreconditions androidPreconditions) {
        return new RecyclerViewAdapter(itemComparator, factoryMap, binderMap, androidPreconditions);
    }

    @Provides
    static ItemComparator provideComparator() {
        return new LoginItemComparator();
    }

    @Binds
    @IntoMap
    @IntKey(LoginConstants.DisplayableTypes.COUNTRY_LINEAR)
    abstract ViewHolderFactory provideCountryLinearViewHolderFactory(CountryLinearViewHolder.Factory factory);

    @Binds
    @IntoMap
    @IntKey(LoginConstants.DisplayableTypes.COUNTRY_LINEAR)
    abstract ViewHolderBinder provideCountryLinearViewHolderBinder(CountryLinearViewHolder.Binder binder);

    @Binds
    @IntoMap
    @IntKey(LoginConstants.DisplayableTypes.COUNTRY_GRID)
    abstract ViewHolderFactory provideCountryGridViewHolderFactory(CountryGridViewHolder.Factory factory);

    @Binds
    @IntoMap
    @IntKey(LoginConstants.DisplayableTypes.COUNTRY_GRID)
    abstract ViewHolderBinder provideCountryGridViewHolderBinder(CountryGridViewHolder.Binder binder);

    @Binds
    @IntoMap
    @IntKey(LoginConstants.DisplayableTypes.SPACER)
    abstract ViewHolderFactory provideSpacerViewHolderFactory(SpacerViewHolder.Factory factory);

    @Binds
    @IntoMap
    @IntKey(LoginConstants.DisplayableTypes.SPACER)
    abstract ViewHolderBinder provideSpacerViewHolderBinder(SpacerViewHolder.Binder binder);

    @Binds
    @IntoMap
    @IntKey(LoginConstants.DisplayableTypes.BRAND_LINEAR)
    abstract ViewHolderFactory provideBrandLinearViewHolderFactory(BrandLinearViewHolder.Factory factory);

    @Binds
    @IntoMap
    @IntKey(LoginConstants.DisplayableTypes.BRAND_LINEAR)
    abstract ViewHolderBinder provideBrandLinearViewHolderBinder(BrandLinearViewHolder.Binder binder);

    @Binds
    @IntoMap
    @IntKey(LoginConstants.DisplayableTypes.BRAND_GRID)
    abstract ViewHolderFactory provideBrandGridViewHolderFactory(BrandGridViewHolder.Factory factory);

    @Binds
    @IntoMap
    @IntKey(LoginConstants.DisplayableTypes.BRAND_GRID)
    abstract ViewHolderBinder provideBrandGridViewHolderBinder(BrandGridViewHolder.Binder binder);

}
