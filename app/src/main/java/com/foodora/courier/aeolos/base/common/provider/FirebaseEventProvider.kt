package com.foodora.courier.aeolos.base.common.provider

import android.os.Bundle
import com.foodora.courier.aeolos.app.application.AeolosApplication
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.perf.metrics.AddTrace
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 23.10.17.
 */

class FirebaseEventProvider
@Inject
internal constructor(app: AeolosApplication) {

    private val firebaseAnalytics = FirebaseAnalytics.getInstance(app)

    @AddTrace(name = "base_common_provider_event")
    fun log(event: String, eventData: Any? = null) {
        val bundle = Bundle()

       // eventData.let {
           // bundle.putString("data", eventData.toString())
        //}

        firebaseAnalytics.logEvent(event, bundle)
        Timber.v(event)
    }

}