package com.foodora.courier.aeolos.base.domain.converter

import android.arch.persistence.room.TypeConverter
import com.google.firebase.perf.metrics.AddTrace
import java.util.*

/**
 * Created by johannes.neutze on 20.09.17.
 */

class DateConverter {

    @AddTrace(name = "base_domain_converter_date_toDate")
    @TypeConverter
    fun toDate(timestamp: Long?): Date? = if (timestamp == null) null else Date(timestamp)

    @AddTrace(name = "base_domain_converter_date_toTimestamp")
    @TypeConverter
    fun toTimestamp(date: Date?): Long? = date?.time
    
}