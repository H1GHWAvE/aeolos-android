package com.foodora.courier.aeolos.base.common.extension


/**
 * Created by johannes.neutze on 20.09.17.
 */

object ListUtils {

    fun <T> union(list1: List<T>, list2: List<T>): List<T> {
        return object : ArrayList<T>() {
            init {
                addAll(list1)
                addAll(list2)
            }
        }
    }

    fun <T> isNotEmpty(list: List<T>): Boolean {
        return !list.isEmpty()
    }
}