package com.foodora.courier.aeolos.login.presentation

import com.foodora.courier.aeolos.base.presentation.recyclerview.DisplayableItem
import com.foodora.courier.aeolos.base.presentation.recyclerview.ItemComparator

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

internal class LoginItemComparator : ItemComparator {

    override fun areItemsTheSame(item1: DisplayableItem<Any>, item2: DisplayableItem<Any>): Boolean {
        return item1 == item2
    }

    override fun areContentsTheSame(item1: DisplayableItem<Any>, item2: DisplayableItem<Any>): Boolean {
        return item1 == item2
    }
}