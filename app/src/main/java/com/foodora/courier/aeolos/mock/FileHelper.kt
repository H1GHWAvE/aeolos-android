package com.foodora.courier.aeolos.mock

import android.content.Context
import com.foodora.courier.aeolos.R
import com.google.firebase.perf.metrics.AddTrace


/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 04.10.17.
 */

object FileHelper {

    @AddTrace(name = "mock_file_getContentFromFile")
    fun getContentFromFile(context: Context): String {
        val inputStream = context.resources.openRawResource(R.raw.mock_endpoints)
        val buffer = ByteArray(inputStream.available())
        while (inputStream.read(buffer) !== -1);

        return String(buffer)
    }

}