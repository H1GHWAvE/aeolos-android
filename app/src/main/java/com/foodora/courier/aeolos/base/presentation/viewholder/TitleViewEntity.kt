package com.foodora.courier.aeolos.base.presentation.viewholder


/**
 * Created by johannes.neutze on 20.09.17.
 */

abstract class TitleViewEntity {
    abstract val title: String
}