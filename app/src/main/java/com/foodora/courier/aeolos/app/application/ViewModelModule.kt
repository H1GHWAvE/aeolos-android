package com.foodora.courier.aeolos.app.application

import com.foodora.courier.aeolos.login.injection.module.LoginViewModelModule
import dagger.Module


/**
 * Created by johannes.neutze on 21.09.17.
 */

@Module(includes = arrayOf(LoginViewModelModule::class))
class ViewModelModule