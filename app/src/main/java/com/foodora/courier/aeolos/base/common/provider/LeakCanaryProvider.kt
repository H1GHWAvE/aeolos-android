package com.foodora.courier.aeolos.base.common.provider

import com.foodora.courier.aeolos.app.application.AeolosApplication
import com.google.firebase.perf.metrics.AddTrace
import com.squareup.leakcanary.LeakCanary
import javax.inject.Inject

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 23.10.17.
 */

class LeakCanaryProvider
@Inject
internal constructor(private var app: AeolosApplication) {

    @AddTrace(name = "base_common_provider_leak")
    fun init() {
        LeakCanary.install(app)
    }

}