package com.foodora.courier.aeolos.login.domain.mappers

import com.foodora.courier.aeolos.base.common.extension.toCountryName
import com.foodora.courier.aeolos.base.common.mapper.Mapper
import com.foodora.courier.aeolos.login.domain.entities.Endpoint
import com.foodora.courier.aeolos.login.network.model.EndpointsResponse
import com.google.firebase.perf.metrics.AddTrace
import java.util.*
import javax.inject.Inject

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 08.08.17.
 */

class CountriesMapper
@Inject
constructor() : Mapper<List<Endpoint>, EndpointsResponse?> {

    @AddTrace(name = "login_domain_countries_map")
    override fun map(input: EndpointsResponse?): List<Endpoint> {
        val countries = mutableListOf<Endpoint>()
        val brands = input?.endpoints ?: ArrayList()

        for (brand in brands) {
            val brandCountry = brand.countries

            brandCountry.mapTo(countries) {
                Endpoint(
                        brand = brand.brand,
                        countryCode = it.countryCode,
                        name = it.countryCode.toCountryName(),
                        url = it.url
                )
            }
        }

        countries.sortBy { it.name }

        return countries
    }
}