package com.foodora.courier.aeolos.login.presentation.endpoint

import com.foodora.courier.aeolos.base.common.extension.toFlagDrawable
import com.foodora.courier.aeolos.base.common.mapper.Mapper
import com.foodora.courier.aeolos.login.domain.entities.Endpoint
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject

/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 21.10.17.
 */

class EndpointViewEntityMapper
@Inject
constructor() : Mapper<EndpointViewEntity?, Endpoint?> {

    @AddTrace(name = "login_endpoint_map")
    override fun map(input: Endpoint?): EndpointViewEntity? {
        return input?.let {
            EndpointViewEntity(
                    brand = input.brand,
                    flag = input.countryCode.toFlagDrawable()
            )
        }
    }
}