package com.foodora.courier.aeolos.base.common.extension

import android.content.Context
import android.util.DisplayMetrics


/**
 * Created by Johannes Neutze
 * johannes (at) foodora.com
 * on 19.11.17.
 */

fun Int.convertToPixel(context: Context): Int {
    val resources = context.resources
    val metrics = resources.displayMetrics
    return Math.round(this * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT))
}

fun Int.convertToDp(context: Context): Int {
    val resources = context.resources
    val metrics = resources.displayMetrics
    return Math.round(this / (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT))
}