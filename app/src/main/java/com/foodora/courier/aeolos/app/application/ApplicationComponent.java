package com.foodora.courier.aeolos.app.application;

import com.foodora.courier.aeolos.app.home.presentation.HomeActivityComponent;
import com.foodora.courier.aeolos.base.injection.module.ActivityModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Created by H1GHWAvE on 26/05/16.
 */

@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                DataModule.class,
                DomainModule.class,
                NetworkModule.class,
                ViewModelModule.class,
        }
)
public interface ApplicationComponent {

    void inject(AeolosApplication app);

    HomeActivityComponent createHomeActivityComponent(ActivityModule module);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(AeolosApplication app);

        ApplicationComponent build();
    }

}
