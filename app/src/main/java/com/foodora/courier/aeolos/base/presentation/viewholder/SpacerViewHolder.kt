package com.foodora.courier.aeolos.base.presentation.viewholder

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.foodora.courier.aeolos.R
import com.foodora.courier.aeolos.base.presentation.recyclerview.DisplayableItem
import com.foodora.courier.aeolos.base.presentation.recyclerview.ViewHolderBinder
import com.foodora.courier.aeolos.base.presentation.recyclerview.ViewHolderFactory
import com.google.firebase.perf.metrics.AddTrace
import javax.inject.Inject

/**
 * Created by johannes.neutze on 20.09.17.
 */

internal class SpacerViewHolder
private constructor(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    internal class Factory
    @Inject
    constructor(context: Context) : ViewHolderFactory(context) {

        @AddTrace(name = "base_spacer_createViewHolder")
        override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding: ViewDataBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.viewholder_base_spacer,
                    parent,
                    false
            )
            return SpacerViewHolder(binding)
        }
    }

    internal class Binder
    @Inject
    constructor() : ViewHolderBinder {
        override fun bind(viewHolder: RecyclerView.ViewHolder, item: DisplayableItem<Any>) {
        }
    }
}