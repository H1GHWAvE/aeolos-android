package com.foodora.courier.aeolos.app.home.domain.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.foodora.courier.aeolos.BuildConfig
import com.foodora.courier.aeolos.base.domain.converter.CalendarConverter
import com.foodora.courier.aeolos.base.domain.converter.DateConverter
import com.foodora.courier.aeolos.login.domain.db.EndpointDao
import com.foodora.courier.aeolos.login.domain.entities.Endpoint
import com.foodora.courier.base.domain.db.SessionDao
import com.foodora.courier.base.domain.entity.Session

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 07.08.17.
 */

@Database(
        entities = arrayOf(
                Endpoint::class,
                Session::class
        ),
        version = BuildConfig.VERSION_CODE,
        exportSchema = false
)
@TypeConverters(DateConverter::class, CalendarConverter::class)
abstract class Database : RoomDatabase() {

    abstract fun countriesDao(): EndpointDao

    abstract fun sessionDao(): SessionDao

}