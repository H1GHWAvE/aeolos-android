package com.foodora.courier.aeolos.base.common.provider

import javax.inject.Inject


/**
 * Created by johannes.neutze on 20.09.17.
 */

class TimestampProvider
@Inject
internal constructor() {

    fun currentTimeMillis(): Long {
        return System.currentTimeMillis()
    }

}