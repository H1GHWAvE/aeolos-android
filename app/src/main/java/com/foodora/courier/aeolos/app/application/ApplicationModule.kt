package com.foodora.courier.aeolos.app.application

import android.app.Application
import dagger.Module
import dagger.Provides


/**
 * Created by H1GHWAvE on 26/05/16.
 */

@Module
class ApplicationModule
internal constructor() {

    @Provides
    internal fun provideApplication(app: AeolosApplication): Application {
        return app
    }

}
