package com.foodora.courier.aeolos.login.ui.presenter

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 10.08.17.
 */

/*
 * pre-condition
 * user action
 * post-condition
 */

fun onBrandSelect(brand: String? = null, position: Int = 0) {
    validateScreenBrand()
    when (brand) {
        null -> selectBrand(position)
        else -> selectBrand(brand)
    }
    validateScreenCountry()
}

fun onCountrySelect(country: String? = null, position: Int = 0) {
    validateScreenCountry()
    when (country) {
        null -> selectCountry(position)
        else -> selectCountry(country)
    }
    validateScreenLogin()
}

fun onEndpointResetCountry() {
    validateScreenCountry()
    resetEndpointCountry()
    validateScreenBrand()
}

fun onEndpointResetLogin() {
    validateScreenLogin()
    resetEndpointLogin()
    validateScreenBrand()
}