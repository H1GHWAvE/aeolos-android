package com.foodora.courier.aeolos.login.ui.presenter

import android.arch.lifecycle.ViewModelProviders
import android.support.test.espresso.Espresso
import android.support.test.espresso.PerformException
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.foodora.courier.aeolos.helper.SimpleIdlingResource
import com.foodora.courier.aeolos.helper.loginBrandFoodora
import com.foodora.courier.aeolos.helper.loginBrandFoodpanda
import com.foodora.courier.aeolos.helper.loginCountryFoodoraGermany
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 09.08.17.
 */

@RunWith(AndroidJUnit4::class)
class LoginActivityInstrumentedTest {

    //TODO all delegates

    // 1. setup conditions for test...
    // 2. Execute code under test...
    // 3. Make assertions on the result...

    @Rule @JvmField
    var activityTestRule = ActivityTestRule(LoginActivity::class.java)

    private val idlingRes = SimpleIdlingResource()


    //region Before

    @Before
    fun idlingResourceSetup() {
        Espresso.registerIdlingResources(idlingRes)
        idlingRes.isIdleNow = false

        val viewModel = ViewModelProviders.of(activityTestRule.activity).get(LoginViewModel::class.java)

        viewModel.brands.observeForever { brands ->
            if (brands != null) {
                idlingRes.isIdleNow = true
            }
        }
    }

    //endregion


    //region UseCases

    @Test
    @Throws(Exception::class)
    fun checkLoginScreen() {
        activityTestRule.activity
        validateToolbar()
    }

    @Test(expected = PerformException::class)
    @Throws(Exception::class)
    fun selectWrongBrand() {
        activityTestRule.activity
        onBrandSelect("brand")
    }

    @Test
    @Throws(Exception::class)
    fun selectFirstBrand_Reset() {
        activityTestRule.activity
        onBrandSelect()
        onEndpointResetCountry()
    }

    @Test
    @Throws(Exception::class)
    fun selectFoodoraBrand_reset() {
        activityTestRule.activity
        onBrandSelect(loginBrandFoodora)
        onEndpointResetCountry()
    }

    @Test(expected = PerformException::class)
    @Throws(Exception::class)
    fun selectFoodoraBrand_selectWrongCountry() {
        activityTestRule.activity
        onBrandSelect(loginBrandFoodpanda)
        onCountrySelect(loginCountryFoodoraGermany)
        onEndpointResetLogin()
    }

    @Test
    @Throws(Exception::class)
    fun selectFirstBrand_selectFirstCountry_reset() {
        activityTestRule.activity
        onBrandSelect()
        onCountrySelect()
        onEndpointResetLogin()
    }

    @Test
    @Throws(Exception::class)
    fun selectFoodoraBrand_selectGermanyCountry_reset() {
        activityTestRule.activity
        onBrandSelect(loginBrandFoodora)
        onCountrySelect(loginCountryFoodoraGermany)
        onEndpointResetLogin()
    }

    //endregion

}