package com.foodora.courier.aeolos.helper

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 10.08.17.
 */

var appPackage = "com.foodora.courier.aeolos"
var appName = "Aeolos"
var loginBrandFoodora = "foodora"
var loginBrandFoodpanda = "foodpanda"
var loginCountryFoodoraGermany = "foodora Germany"