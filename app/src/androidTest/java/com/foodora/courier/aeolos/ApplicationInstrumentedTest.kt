package com.foodora.courier.aeolos

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.foodora.courier.aeolos.helper.appPackage
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 10.08.17.
 */


@RunWith(AndroidJUnit4::class)
class ApplicationInstrumentedTest {

    @Test
    @Throws(Exception::class)
    fun useAppContext() {
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals(appPackage, appContext.packageName)
    }

}