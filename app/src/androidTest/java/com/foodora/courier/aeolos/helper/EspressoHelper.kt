package com.foodora.courier.aeolos.helper

import android.support.annotation.IdRes
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.v7.widget.RecyclerView
import org.hamcrest.Matcher

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 10.08.17.
 */

fun isViewDisplayed(@IdRes resourceId: Int) {
    onView(withId(resourceId)).check(matches(isDisplayed(resourceId)))
}

fun isTextDisplayed(@IdRes resourceId: Int, text: String) {
    onView(withId(resourceId)).check(matches(withText(text)))
}

fun click(@IdRes resourceId: Int) {
    onView(withId(resourceId)).perform(click())
}

fun clickRecycler(@IdRes resourceId: Int, position: Int) {
    onView(withId(resourceId)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(position, click()))
}

fun clickRecycler(@IdRes resourceId: Int, matcher: Matcher<RecyclerView.ViewHolder>) {
    onView(withId(resourceId)).perform(RecyclerViewActions.scrollToHolder(matcher)).perform(click())
}

