package com.foodora.courier.aeolos.helper

import android.support.annotation.IdRes
import android.support.test.InstrumentationRegistry

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 10.08.17.
 */

fun resourceName(@IdRes resourceId: Int): String {
    return InstrumentationRegistry.getTargetContext().resources.getResourceEntryName(resourceId)
}