package com.foodora.courier.aeolos.login.ui.presenter

import com.foodora.courier.aeolos.R
import com.foodora.courier.aeolos.helper.*

/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 10.08.17.
 */

fun selectBrand(position: Int) {
    clickRecycler(R.id.recycler_login_container_brand, position)
}

fun selectBrand(brand: String) {
    clickRecycler(R.id.recycler_login_container_brand, withBrandViewHolder(brand))
}

fun selectCountry(position: Int) {
    clickRecycler(R.id.recycler_login_container_country, position)
}

fun selectCountry(country: String) {
    clickRecycler(R.id.recycler_login_container_country, withCountryViewHolder(country))
}

fun resetEndpointCountry() {
    click(R.id.button_login_container_country)
}

fun resetEndpointLogin() {
    click(R.id.button_login_container_login)
}

fun validateScreenBrand() {
    isViewDisplayed(R.id.recycler_login_container_brand)
}

fun validateScreenCountry() {
    isViewDisplayed(R.id.recycler_login_container_country)
    isViewDisplayed(R.id.button_login_container_country)
}

fun validateScreenLogin() {
    isViewDisplayed(R.id.button_login_container_login)
}

fun validateToolbar() {
    isViewDisplayed(R.id.toolbar_login)
    isViewDisplayed(R.id.text_toolbar_login)
    isTextDisplayed(R.id.text_toolbar_login, appName)
}