package com.foodora.courier.aeolos.helper

import android.graphics.Rect
import android.support.annotation.IdRes
import android.support.test.espresso.matcher.BoundedMatcher
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.foodora.courier.aeolos.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher


/**
 * Created by johannes.neutze
 * johannes (at) foodora.com
 * on 10.08.17.
 */

fun withBrandViewHolder(text: String): Matcher<RecyclerView.ViewHolder> {
    return object : BoundedMatcher<RecyclerView.ViewHolder, BrandAdapter.BrandViewHolder>(BrandAdapter.BrandViewHolder::class.java) {

        override fun describeTo(description: Description) {
            description.appendText("No BrandGridViewHolder found with text: $text")
        }

        override fun matchesSafely(item: BrandAdapter.BrandViewHolder): Boolean {
            val timeViewText = item.itemView.findViewById<TextView>(R.id.text_content_viewholder_adapter)
            return timeViewText.text.toString() == text
        }
    }
}

fun withCountryViewHolder(text: String): Matcher<RecyclerView.ViewHolder> {
    return object : BoundedMatcher<RecyclerView.ViewHolder, CountryAdapter.CountryViewHolder>(CountryAdapter.CountryViewHolder::class.java) {

        override fun describeTo(description: Description) {
            description.appendText("No CountryLinearViewHolder found with text: $text")
        }

        override fun matchesSafely(item: CountryAdapter.CountryViewHolder): Boolean {
            val timeViewText = item.itemView.findViewById<TextView>(R.id.text_content_viewholder_adapter)
            return timeViewText.text.toString() == text
        }
    }
}

fun isDisplayed(@IdRes resourceId: Int): Matcher<View> {
    return object : TypeSafeMatcher<View>() {

        override fun describeTo(description: Description) {
            description.appendText("${resourceName(resourceId)} is displayed on the screen to the user")
        }

        override fun matchesSafely(view: View): Boolean {
            return view.getGlobalVisibleRect(Rect()) && withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE).matches(view)
        }
    }
}