// Config
var LANGUAGES = ['de'];
var SPREADSHEET_KEY = "1gPhw59eoWvUNvj4XEKV6kFyklP5aem5-S55rjxRtkB0";
var basePath = "app/src/main/res/";


// Script
var Localize = require("localize-with-spreadsheet");
var transformer = Localize.fromGoogleSpreadsheet(SPREADSHEET_KEY, 'Android');
transformer.setKeyCol('KEY');


// EN is base
transformer.save(basePath + "values/strings.xml", { valueCol: "EN", format: "android" });

LANGUAGES.forEach(function (lang) {
  transformer.save(basePath + "values-" + lang + "/strings.xml", { valueCol: lang.toUpperCase(), format: "android" });
});
